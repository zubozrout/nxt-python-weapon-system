#!/usr/bin/python

import nxt.locator
from nxt.sensor import *

class DistanceEOPD():	
	# constructor
	def __init__(self, device, port):
		self.device = device		
		self.port = port		
		self.sensor = nxt.sensor.hitechnic.EOPD(self.device, self.port)
		self.sensor.set_range_long()

	# get raw value
	def getRawValue(self):
		return self.sensor.get_raw_value()
	
	# get processed value
	def getProcessedValue(self):
		return self.sensor.get_processed_value()
	
	# get scaled value
	def getScaledValue(self):
		return self.sensor.get_scaled_value()
