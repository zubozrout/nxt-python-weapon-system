#!/usr/bin/python

import nxt.locator
from nxt.sensor import *

class Angle():
	# constructor
	def __init__(self, device, port):
		self.device = device		
		self.port = port		
		self.sensor = nxt.sensor.hitechnic.Angle(self.device, self.port)

	# get current angle
	def getAngle(self):
		return self.sensor.get_angle()
	
	# get accumulated angle
	def getAccumulatedAngle(self):
		return self.sensor.get_accumulated_angle()
