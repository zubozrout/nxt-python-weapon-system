#!/usr/bin/python

from threading import Thread
import signal
import sys

#!/usr/bin/python

import nxt.locator
from nxt.motor import *

class CustomThreads():
	threads = []
	
	# constructor
	def __init__(self):
		signal.signal(signal.SIGTERM, self.joinThreads)

	# Called upon SIGTERM, joining all running threads
	def joinThreads(self):
		global threads
		
		print("Preparing to exit main thread ...")
		# Notify threads it's time to exit
		exitFlag = 1
		
		# Wait for all threads to complete
		for t in threads:
			t.join()
		print("Main thread done.")

	# Run a passed function with passed args (in array) in a new thread
	def prepareThread(self, function, args):
		thread = Thread(target=function, args=args)
		self.threads.append(thread)
		return thread
