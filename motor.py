#!/usr/bin/python

import nxt.locator
from nxt.motor import *

import time

class CustomMotor():
	speed = 20
	angle = 10
	
	# constructor
	def __init__(self, device, port):
		self.device = device
		self.port = port
		self.motor = Motor(self.device, self.port)

	# run motor
	def run(self, speed, angle):
		self.motor.turn(speed, angle)
	
	# stop motor
	def stop(self):
		self.motor.brake()
		time.sleep(0.2)
		self.motor.idle()
		self.motor.reset_position(True)
	
	# run motor by step
	def step(self, directionPositive):
		if directionPositive == True:
			self.run(self.speed, self.angle)
		else:
			self.run(-self.speed, self.angle)
		
	# get motor angle
	def getAngle(self):
		return 0


