#!/usr/bin/python

import nxt.locator
from nxt.sensor import *

class DistanceSensor():
	# constructor
	def __init__(self, device, port):
		self.device = device
		self.port = port
		self.device = Ultrasonic(self.device, self.port)

	# get distance
	def getDistance(self):
		return self.device.get_sample()


