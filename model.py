#!/usr/bin/python

import math

class MathModel():
	# constructor
	def __init__(self):
		self.model = ""

	# get Z from object distance and vertical Angle (up |)
	def getZ(self, objectDistance, verticalAngle):
		return math.cos(verticalAngle) * objectDistance

	# get diagonal surface distance (DSD) from object distance and vertical Angle (flat \)
	def getDiagonalSurfaceDistance(self, objectDistance, verticalAngle):
		return math.sin(verticalAngle) * objectDistance

	# get Y from DSD and surface Angle (flat |)
	def getY(self, DSD, surfaceAngle):
		return math.sin(surfaceAngle) * DSD

	# get X from DSD and surface Angle (flat -)
	def getX(self, DSD, surfaceAngle):
		return math.cos(surfaceAngle) * DSD

	# get X, Y, Z from object distance, surface Angle and vertical Angle
	def getSpacePosition(self, objectDistance, surfaceAngle, verticalAngle):
		posZ = self.getZ(objectDistance, verticalAngle)
		surfaceDistance = self.getDiagonalSurfaceDistance(objectDistance, verticalAngle)
		posY = self.getY(surfaceDistance, surfaceAngle)
		posX = self.getX(surfaceDistance, surfaceAngle)

		return {
			"x": posX,
			"y": posY,
			"z": posZ
		}

