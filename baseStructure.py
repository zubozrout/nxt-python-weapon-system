#!/usr/bin/python

import nxt.locator
from nxt.motor import *
from nxt.sensor import *

import motor as motor
import distanceSensor as ds

import angle
import eopd
import light
import model

import time

import math
import plot

class BaseStructure():
	initX = 0
	initY = 0
	
	x = 0
	y = 0
	
	leftRight = {
		"right": 0,
		"left": 0
	}
	
	upDown = {
		"front": 0,
		"back": 0
	}
	
	turnLeftRight = 10500
	turnUpDown = 400
	
	ldirClock = True
	
	closestDistance = {
		"distance": -1,
		"x": 0,
		"y": 0
	}
	
	# constructor
	def __init__(self, device, threadsManager):
		self.device = device
		self.threadsManager = threadsManager
		
		self.baseMotorLeftRight = motor.CustomMotor(self.device, PORT_C)
		self.baseMotorUpDown = motor.CustomMotor(self.device, PORT_B)
		
		self.motorPump = motor.CustomMotor(self.device, PORT_A)
		
		self.angleLeftRight = angle.Angle(self.device, PORT_3)
		self.angleUpDown = angle.Angle(self.device, PORT_4)
		
		#self.eopdInstance = eopd.DistanceEOPD(self.device, PORT_1)
		self.dsInstance = ds.DistanceSensor(self.device, PORT_1)
		
		self.mathModel = model.MathModel()
	
	# init + calibration	
	def init(self):
		self.calibrateLeftRight()
		self.calibrateUpDown()
		self.baseMotorUpDown.run(50, self.turnUpDown)
		
	# print current positions
	def motorTestDrive(self):
		speed = 100
		step = 10000
		
		testMotor = motor.CustomMotor(self.device, PORT_C)
		thread = self.threadsManager.prepareThread(testMotor.run, (speed, step))
		thread.start()
		testMotor.stop()
		thread.join()
	
	# print current positions
	def printCurrentLocation(self):
		print("X: " + str(self.x))
		print("Y: " + str(self.y))
	
	def bordersLeftRight(self, ldif):
		print(str(ldif) + ", left: " + str(self.leftRight["left"]))
		print(str(ldif) + ", right: " + str(self.leftRight["right"]))
		return {
			"left": ldif + 20 > self.leftRight["left"],
			"right": ldif - 20 < self.leftRight["right"]
		}
	
	# Check if can go left
	def canGoLeft(self, pos):
		return not self.bordersLeftRight(pos)["left"]
	
	# Check if can go right
	def canGoRight(self, pos):
		return not self.bordersLeftRight(pos)["right"]
	
	def bordersUpDown(self, udif):
		print(str(udif) + ", front: " + str(self.upDown["front"]))
		print(str(udif) + ", back: " + str(self.upDown["back"]))
		return {
			"front": udif + 10 > self.upDown["front"],
			"back": udif - 10 < self.upDown["back"]
		}
	
	# Check if can go forwards
	def canGoForwads(self, pos):
		return not self.bordersUpDown(pos)["front"]
	
	# Check if can go backwards
	def canGoBackwards(self, pos):
		return not self.bordersUpDown(pos)["back"]
	
	# Print current motor positions with intervals in a readable format
	def printCurrentMotorPositions(self):
		ldif = self.angleLeftRight.getAccumulatedAngle()
		udif = self.angleUpDown.getAccumulatedAngle()
		print("l/r: " + str(ldif) + ": [" + str(self.leftRight["left"]) + ", " + str(self.leftRight["right"]) + "]")
		print("u/d: " + str(udif) + ": [" + str(self.upDown["front"]) + ", " + str(self.upDown["back"]) + "]")
	
	def calibrateLeftRight(self):
		print("calibrate left right")
		speed = 100
		
		self.baseMotorLeftRight.run(speed, self.turnLeftRight / 2)
		time.sleep(0.1)
		self.leftRight["right"] = self.angleLeftRight.getAccumulatedAngle()
		
		self.baseMotorLeftRight.run(-speed, self.turnLeftRight)
		time.sleep(0.1)
		self.leftRight["left"] = self.angleLeftRight.getAccumulatedAngle()
		
		self.printCurrentMotorPositions()
	
	def calibrateUpDown(self):
		print("calibrate up down")
		speed = 10
		
		self.baseMotorUpDown.run(speed, self.turnUpDown / 2)
		time.sleep(0.1)
		self.upDown["front"] = self.angleUpDown.getAccumulatedAngle()
		
		self.baseMotorUpDown.run(-speed, self.turnUpDown)
		time.sleep(0.1)
		self.upDown["back"] = self.angleUpDown.getAccumulatedAngle()
		
		self.printCurrentMotorPositions()
	
	# Update closest distance if any present
	def updateClosestDistance(self):
		distance = self.dsInstance.getDistance()
		
		if distance != 0:
			if self.closestDistance["distance"] == -1:
				self.closestDistance["distance"] = distance
				self.closestDistance["x"] = self.angleLeftRight.getAccumulatedAngle()
				self.closestDistance["y"] = self.angleUpDown.getAccumulatedAngle()
			
			if distance < self.closestDistance["distance"]:
				self.closestDistance["distance"] = distance
				self.closestDistance["x"] = self.angleLeftRight.getAccumulatedAngle()
				self.closestDistance["y"] = self.angleUpDown.getAccumulatedAngle()
		
		print("current distance: " + str(distance))
		#print("closest: " + str(self.closestDistance["distance"]))
	
	def actionOnRun(self):
		fireRunSpeed = 100
		fireRunAngle = 5000
		fireOnDistanceLowerThan = 10
		
		self.updateClosestDistance()
				
		if self.dsInstance.getDistance() < fireOnDistanceLowerThan:
			self.motorPump.run(fireRunSpeed, fireRunAngle)
		
		# Math model + plot rendering
		diffLR = abs(float(self.leftRight["left"] - self.leftRight["right"]))
		if self.leftRight["left"] > self.leftRight["right"]:
			diffLR = abs(float(self.leftRight["right"] - self.leftRight["left"]))
		dergeeLR = float(diffLR/180)
		
		currentFromLeft = abs(self.angleLeftRight.getAccumulatedAngle() + self.leftRight["left"])/dergeeLR
		if self.leftRight["left"] > 0:
			currentFromLeft = abs(self.angleLeftRight.getAccumulatedAngle() - self.leftRight["left"])/dergeeLR
		
		diffUD = abs(float(self.upDown["front"] - self.upDown["back"]))
		if self.upDown["front"] > self.upDown["back"]:
			diffUD = abs(float(self.upDown["back"] - self.upDown["front"]))
		dergeeUD = float(diffUD/180)
		
		currentFromFront = abs(self.angleUpDown.getAccumulatedAngle() + self.upDown["front"])/dergeeUD
		if self.upDown["front"] > 0:
			currentFromFront = abs(self.angleUpDown.getAccumulatedAngle() - self.upDown["front"])/dergeeUD
		
		pos = self.mathModel.getSpacePosition(self.dsInstance.getDistance(), currentFromLeft, currentFromFront)
		print(pos)
		plot.render(pos["x"], pos["y"], pos["z"])
		plot.plt.pause(0.25)
	
	# turn base left-right
	def turnBaseLeftRight(self):
		speed = 100
		step = 10000
		
		ldif = self.angleLeftRight.getAccumulatedAngle()
		
		# Chci dojet az na konec a kontrolovat. Jakmile dojedu na konec, zmenit smer a jet zase na konec.
		if self.canGoLeft(ldif):
			print("muzu doleva")
			
			ldif = self.angleLeftRight.getAccumulatedAngle()
			print("goes left")
			ltr = self.threadsManager.prepareThread(self.baseMotorLeftRight.run, (-speed, step)) # po smeru hodin (doleva)
			ltr.start()
			
			while ltr.isAlive():
				self.actionOnRun()
				time.sleep(0.2)
			
			ltr.join()
			return
		
		if self.canGoRight(ldif):
			print("muzu doprava")
			
			ldif = self.angleLeftRight.getAccumulatedAngle()
			print("goes right")
			ltr = self.threadsManager.prepareThread(self.baseMotorLeftRight.run, (speed, step)) # proti hodinam (doprava)
			ltr.start()
			
			while ltr.isAlive():
				self.actionOnRun()
				time.sleep(0.2)
			
			ltr.join()
			return
		
	# turn base up-down
	def turnBaseUpDown(self):
		speed = 10
		step = 50
		
		udif = self.angleUpDown.getAccumulatedAngle()
		
		# Chci dojet az na konec a kontrolovat. Jakmile dojedu na konec, zmenit smer a jet zase na konec.
		if self.canGoForwads(udif):
			udif = self.angleUpDown.getAccumulatedAngle()
			print("goes up")
			ltr = self.threadsManager.prepareThread(self.baseMotorUpDown.run, (speed, step)) # jed nahoru (dopredu)
			ltr.start()
			ltr.join()
			self.printCurrentMotorPositions()
		
		if self.canGoBackwards(udif):
			udif = self.angleUpDown.getAccumulatedAngle()
			print("goes down")
			ltr = self.threadsManager.prepareThread(self.baseMotorUpDown.run, (-speed, step)) # jed dozadu
			ltr.start()
			ltr.join()
			self.printCurrentMotorPositions()
	
	# Go through all turns
	def cicleAll(self):
		speed = 100
		
		for x in range(0, 4):
			print("cycle")
			self.turnBaseLeftRight()
			
			self.baseMotorUpDown.run(-speed, self.turnUpDown / 4)
			time.sleep(0.1)
			self.upDown["front"] = self.angleUpDown.getAccumulatedAngle()
		
		# Up/Down		
		self.baseMotorUpDown.run(speed, self.turnUpDown)
		time.sleep(0.1)
		self.upDown["back"] = self.angleUpDown.getAccumulatedAngle()
		
