#!/usr/bin/python

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np

import math

# Plot
def randrange(n, vmin, vmax):
    '''
    Helper function to make an array of random numbers having shape (n, )
    with each number distributed Uniform(vmin, vmax).
    '''
    return (vmax - vmin)*np.random.rand(n) + vmin

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

n = 100

def distance_a(sinX, c):
	return math.sin(sinX) * c

def distance_b(cosX, c):
	return math.cos(cosX) * c

def render(x, y, z):
	#ax.clear()
	# For each set of style and range settings, plot n random points in the box
	# defined by x in [23, 32], y in [0, 100], z in [zlow, zhigh].
	
	xs = x
	ys = y
	zs = z
	ax.scatter(xs, ys, zs, c='r', marker='o')
	
	ax.set_xlabel('X Label')
	ax.set_ylabel('Y Label')
	ax.set_zlabel('Z Label')
	plt.draw()

plt.ion()
