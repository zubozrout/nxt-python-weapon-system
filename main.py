#!/usr/bin/python

import nxt.locator
import nxt.brick
import threads as th
import baseStructure
import time

# Init:
threadsManager = th.CustomThreads()

device = nxt.locator.find_one_brick()
baseStructure = baseStructure.BaseStructure(device, threadsManager)

#baseStructure.motorPump.run(100, 5000)

#baseStructure.baseMotorUpDown.run(10, 100)
baseStructure.baseMotorLeftRight.run(100, 5000)

baseStructure.init()

while True:
	try:
		baseStructure.cicleAll()
	except:
		print("Runtime error!")
		break
	time.sleep(1)

