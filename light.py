#!/usr/bin/python

import nxt.locator

class LightBulb():	
	# constructor
	def __init__(self, device, port):
		self.device = device		
		self.port = port		
		self.light = nxt.sensor.Light(self.device, self.port)

	# light up
	def up(self):
		return self.light.set_illuminated(True)
	
	# power down
	def down(self):
		return self.light.set_illuminated(False)
